import { TestBed, inject } from '@angular/core/testing';

import { BitbayService } from './bitbay.service';

describe('BitbayService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BitbayService]
    });
  });

  it('should be created', inject([BitbayService], (service: BitbayService) => {
    expect(service).toBeTruthy();
  }));
});
