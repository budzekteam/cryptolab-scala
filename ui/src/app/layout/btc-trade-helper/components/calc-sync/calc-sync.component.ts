import { Component, OnInit } from '@angular/core';
import {CalcSheetRowModel} from "../calc-sheet/calc-sheet-row/calc-sheet-row.component";

@Component({
  selector: 'app-calc-sync',
  templateUrl: './calc-sync.component.html',
  styleUrls: ['./calc-sync.component.scss']
})
export class CalcSyncComponent implements OnInit {

  positions: Array<CalcSheetRowModel> = this.getMockModels();

  constructor() { }

  ngOnInit() {
  }

    getMockModels(): Array<CalcSheetRowModel> {
        return [CalcSheetRowModel.empty(), CalcSheetRowModel.empty(), CalcSheetRowModel.all(5)];
    }

}
