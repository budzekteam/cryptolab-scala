import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartsSheetComponent } from './charts-sheet.component';

describe('ChartsSheetComponent', () => {
  let component: ChartsSheetComponent;
  let fixture: ComponentFixture<ChartsSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartsSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartsSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
