import {Component, forwardRef, Input, OnInit} from "@angular/core";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

const NUMBER_CONTROL_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InfoFieldComponent),
    multi: true
};

@Component({
    selector: 'app-info-field',
    // seems we can't use OnPush, cause it won't update the field in UI, when we patch with emmitEvent = false
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './info-field.component.html',
    providers: [NUMBER_CONTROL_ACCESSOR],

    styleUrls: ['./info-field.component.scss']
})
export class InfoFieldComponent<T> implements OnInit, ControlValueAccessor {

    @Input() locked: boolean = false;
    @Input() maxDecimal: number = 2;
    @Input() format: string = "1.2-" + this.maxDecimal;
    @Input() useFormatting: boolean = false;


    value:T;

    private onTouch: Function;
    private onModelChange: Function;

    constructor() {
    }

    ngOnInit() {
    }

    onChange(value: T){
        // console.log("onChange invoked " + value);

        this.value = value;
        this.onModelChange(value);
    }

    //from ControlValueAccessor interface
    writeValue(value: T): void {
        // console.log("writeValue invoked " + value);

        this.value = value;
    }

    //from ControlValueAccessor interface
    registerOnChange(fn: Function): void {
        this.onModelChange = fn;
    }

    //from ControlValueAccessor interface
    registerOnTouched(fn: Function): void {
        this.onTouch = fn;
    }
}
