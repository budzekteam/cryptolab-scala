import {Component, OnInit} from '@angular/core';
import {StockService} from "app/services";
import {Observable} from "rxjs/Observable";



@Component({
    selector: 'app-btc-trade-helper',
    templateUrl: './btc-trade-helper.component.html',
    styleUrls: ['./btc-trade-helper.component.scss']
})
export class BtcTradeHelperComponent implements OnInit {


    currentExchangePlnBtc$: Observable<number>;

    public seriesAData() : Array<any> {
        this.stockServie.currentExchangePlnBtc();
        return [];
    }

    constructor(private stockServie : StockService) {
        this.currentExchangePlnBtc$ = stockServie.currentExchangePlnBtc();
    }

    ngOnInit() {
    }

}
