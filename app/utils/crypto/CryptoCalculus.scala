package utils.crypto

import akka.actor.ActorSystem

import scala.concurrent.Future
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.{ HttpRequest, HttpResponse, Uri }
import akka.http.scaladsl.unmarshalling.{ Unmarshal, Unmarshaller }
import akka.stream.{ ActorMaterializer, Materializer }
import akka.stream.scaladsl.{ Flow, Source }
import com.google.common.base.Ticker
import spray.json.DefaultJsonProtocol
import utils.crypto.CurrencySupport.CurrencyNotSupportedException

import scala.concurrent.ExecutionContext.Implicits.global

//implicit val ec : ExecutionContext;

object Currency extends Enumeration {
  import scala.language.implicitConversions

  //  protected case class Val(val name: String, val acronym: String) extends super.Val {
  case class Val(val name: String, val acronym: String) extends super.Val {
  }

  implicit def valueToCurrencyVal(x: Value): Val = x.asInstanceOf[Val]

  val USDT = Val("USDTether", "USDT")
  val PLN = Val("PLN", "PLN")
  val BTC = Val("Bitcoin", "BTC")
  val LTC = Val("Litecoin", "LTC")
  val ETH = Val("Ethereum", "ETH")
  val PAY = Val("TenX PAY", "PAY")
  val ADX = Val("AdEx", "ADX")
  val NEO = Val("NEO", "NEO")
  val XLM = Val("Stellar Lumen", "XLM")

  def apply(string: String) = {
    string.replaceAll(" ", "").split(",").map(s => Currency.withName(s)).toList
  }
}

object Operation extends Enumeration {
  val BID = Value
  val ASK = Value
}

//object $ extends Currency

trait Api {
  val currencySupport: CurrencySupport
}

/**
 * Both FutureBasedApi and FlowBasedApi are at the request-level-api, while being based on Feature's or Flow's respectively.
 *
 */
trait FutureBasedPublicApi extends Api {

  def getPrice(c1: Currency.Value, c2: Currency.Value, operation: Operation.Value): Future[Double]

  def currentPrice(from: Currency.Value, to: Currency.Value): Future[Double] = {
    if (currencySupport.supports_(from, to)) {
      getPrice(from, to, Operation.BID)
    } else if (currencySupport.supports_(to, from)) {
      getPrice(from, to, Operation.ASK).map(x => 1 / x)
    } else {
      throw new CurrencyNotSupportedException()
    }
  }

  def currentAmountPrice(from: Currency.Value, to: Currency.Value, amount: Double): Future[Double] = {
    currentPrice(from, to).map(price => price * amount)
  }
}
trait FlowBasedPublicApi extends {
  def currentPrice$(from: Currency.Value, to: Currency.Value): Source[Double, akka.NotUsed]
}

trait FutureBasedPrivateApi extends {
}
trait FlowBasedPrivateApi extends {
}

trait PublicApi extends FutureBasedPublicApi with FlowBasedPublicApi {
  def currentPrice(from: Currency.Value, to: Currency.Value): Future[Double]
}

trait PrivateApi extends FutureBasedPublicApi with FutureBasedPrivateApi {
}

trait CurrencySupport {
  val markets: Map[Currency.Value, List[Currency.Value]]
  val customCurrencyAcronym: Map[Currency.Value, String] = Map() //TODO acronym can be different on different exchanges, consider this

  def supports(from: Currency.Value, to: Currency.Value): Boolean = {
    supports_(from, to) || supports_(to, from)
  }

  def supports_(from: Currency.Value, to: Currency.Value): Boolean = {
    markets.get(from).map(list => list.contains(to)).isDefined
  }
}

object CurrencySupport {
  class CurrencyNotSupportedException extends Exception
}

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  case class Ticker(bid: Double, ask: Double, last: Double)
  implicit val tickerFormat = jsonFormat3(Ticker)

  implicit val system = ActorSystem("QuickStart")
  implicit val materializer = ActorMaterializer()

  def toTicker(httpResponse: HttpResponse): Future[Ticker] = {
    Unmarshal(httpResponse.entity).to[Ticker]
  }
}

trait ExchangeProvider extends PublicApi with PrivateApi with JsonSupport {
  val $ = Currency
  val http = Http()

  def getPriceRequestUri(c1: Currency.Value, c2: Currency.Value): Uri

  def getPriceRequest(c1: Currency.Value, c2: Currency.Value): HttpRequest = {
    HttpRequest(method = GET, uri = getPriceRequestUri(c1, c2))
  }

  def getPrice(c1: Currency.Value, c2: Currency.Value, operation: Operation.Value): Future[Double] = {
    http.singleRequest(getPriceRequest(c1, c2)).flatMap(response => Unmarshal(response.entity).to[Ticker]).map(ticker => ticker.ask)
  }
}

class BittrexProvider extends ExchangeProvider {

  override val currencySupport = new CurrencySupport {
    //TODO ofc move this to an external configuration file xD
    override val markets = Map[Currency.Value, List[Currency.Value]](
      //    override val markets = Map(
      $.BTC -> $("PAY,ADX,XLM,POT,ETH,NEO"),
      $.USDT -> $("BTC,PAY,ADX,XLM,POT,ETH,NEO"),
      $.ETH -> $("PAY,ADX,XLM,POT,ETH,NEO")
    )
  }
  def getUri(action: String, params: String): Uri = {
    Uri(s"https://bittrex.com/api/v1.1/public/$action?$params")
  }

  def getPriceRequestUri(c1: Currency.Value, c2: Currency.Value): Uri = {
    getUri("getticker", "markets=BTC-LTC")
  }

  def getPrice$(c1: Currency.Value, c2: Currency.Value, operation: Operation.Value): Future[Double] = {
    Future.successful(0)
  }

  override def currentPrice$(from: Currency.Value, to: Currency.Value) = ???
}

object CryptoCalculus {
  implicit val exchangeProvider: ExchangeProvider = new BittrexProvider();

  def exchangeValue(from: Currency.Value, to: Currency.Value)(value: Double)(implicit exchangeProvider: ExchangeProvider): Future[Double] = {
    return exchangeProvider.currentPrice(from, to).map(exchangeRate => exchangeRate * value)
  }
}
