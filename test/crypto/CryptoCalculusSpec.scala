package crypto

import org.scalatest.concurrent.ScalaFutures
import org.scalatest._
import Matchers._
import utils.crypto.{ CryptoCalculus, Currency }

import scala.concurrent.{ Await, Future }

class RoutingSpec extends UnitSpec {
  "A visitor count controller" when {
    "user ask for current visitors count" should {
      "return proper number" in {
        val result: Future[Double] = CryptoCalculus.exchangeValue(Currency.NEO, Currency.BTC)(5);
        whenReady(result) { result =>
          result shouldBe a[Double]
        }
      }
    }
  }
}

class UnitSpec extends WordSpec with MustMatchers with ScalaFutures with FunSuite {
  def futureResults[T](f: Future[T]): T = Await.result(f, 5 seconds)
}

